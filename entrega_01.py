# Mensaxe de benvida

def mensaxe_benvida():
	print("""
Welcome to fdisk (util-linux 2.27.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

	""")

# Menu para a opcion F

def menu_F():
	print("""
Unpartitioned space /dev/sda: 0 B, 0 bytes, 0 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
	""")

def menu_exp_d ():
	print("""
First sector: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  2f 17 88 4c 00 00 80 20
000001c0  21 00 83 fe ff ff 00 08  00 00 00 f8 df 02 00 fe
000001d0  ff ff 05 fe ff ff fe 07  e0 02 02 f0 3f 00 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa
	""")

def menu_exp_D ():
	print("""
	EBR: offset = 24697109504, size = 512 bytes.
5c00ffc00  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
5c00ffdb0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 fe
5c00ffdc0  ff ff 82 fe ff ff 02 00  00 00 00 f0 3f 00 00 00
5c00ffdd0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
5c00ffdf0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa
	""")



# Menu para a opcion l

def menu_l():
	print("""
 0  Empty           24  NEC DOS         81  Minix / old Lin bf  Solaris
 1  FAT12           27  Hidden NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extended  c7  Syrinx
 5  Extended        41  PPC PReP Boot   86  NTFS volume set da  Non-FS data
 6  FAT16           42  SFS             87  NTFS volume set db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Dell Utility
 8  AIX             4e  QNX4.x 2nd part 8e  Linux LVM       df  BootIt
 9  AIX bootable    4f  QNX4.x 3rd part 93  Amoeba          e1  DOS access
 a  OS/2 Boot Manag 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad hi ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  Hidden FAT12    56  Golden Bow      a8  Darwin UFS      f0  Linux/PA-RISC b
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  Hidden FAT16 <3 61  SpeedStor       ab  Darwin boot     f4  SpeedStor
16  Hidden FAT16    63  GNU HURD or Sys af  HFS / HFS+      f2  DOS secondary
17  Hidden HPFS/NTF 64  Novell Netware  b7  BSDI fs         fb  VMware VMFS
18  AST SmartSleep  65  Novell Netware  b8  BSDI swap       fc  VMware VMKCORE
1b  Hidden W95 FAT3 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  Hidden W95 FAT3 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  Hidden W95 FAT1 80  Old Minix       be  Solaris boot    ff  BBT
	""")

def menu_p():
	print("""
Disk /dev/sda: 25 GiB, 26843545600 bytes, 52428800 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x4c88172f

Device     Boot    Start      End  Sectors Size Id tipo
/dev/sda1  *        2048 48234495 48232448  23G 83 Linux
/dev/sda2       48236542 52426751  4190210   2G  5 Extended
/dev/sda5       48236544 52426751  4190208   2G 82 Linux swap / Solaris
	""")

def menu_p_exp():
	print("""
Disk /dev/sda: 25 GiB, 26843545600 bytes, 52428800 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x4c88172f

Device     Boot    Start      End  Sectors Id tipo    Start-C/H/S   End-C/H/S Attrs
/dev/sda1  *        2048 48234495 48232448 83 Linux       0/33/32 1023/63/254    80
/dev/sda2       48236542 52426751  4190210  5 Extende 1023/63/254 1023/63/254
/dev/sda5       48236544 52426751  4190208 82 Linux s 1023/63/254 1023/63/254
	""")

# Menus info das particións

def menu_info_sda1():
	print("""
    Device: /dev/sda1
           Boot: *
          Start: 2048
            End: 48234495
        Sectors: 48232448
      Cylinders: 3003
           Size: 23G
             Id: 83
           Type: Linux
    Start-C/H/S: 0/33/32
      End-C/H/S: 1023/63/254
          Attrs: 80
	""")


def menu_info_sda2():
	print("""
    Device: /dev/sda2
          Start: 48236542
            End: 52426751
        Sectors: 4190210
      Cylinders: 261
           Size: 2G
             Id: 5
           Type: Extended
    Start-C/H/S: 1023/63/254
      End-C/H/S: 1023/63/254
	""")

def menu_info_sda5():
	print("""

	Device: /dev/sda5
          Start: 48236544
            End: 52426751
        Sectors: 4190208
      Cylinders: 261
           Size: 2G
             Id: 82
           Type: Linux swap / Solaris
    Start-C/H/S: 1023/63/254
      End-C/H/S: 1023/63/254
	""")



# Menu para a petición de insercion dunha opcion.

def menu_opcion():
	print("""Help:

	  DOS (MBR)
	   a   toggle a bootable flag
	   b   edit nested BSD disklabel
	   c   toggle the dos compatibility flag

	  Generic
	   d   delete a partition
	   F   list free unpartitioned space
	   l   list known partition types
	   n   add a new partition
	   p   print the partition table
	   t   change a partition type
	   v   verify the partition table
	   i   print information about a partition

	  Misc
	   m   print this menu
	   u   change display/entry units
	   x   extra functionality (experts only)

	  Script
	   I   load disk layout from sfdisk script file
	   O   dump disk layout to sfdisk script file

	  Save & Exit
	   w   write table to disk and exit
	   q   quit without saving changes

	  Create a new label
	   g   create a new empty GPT partition table
	   G   create a new empty SGI (IRIX) partition table
	   o   create a new empty DOS partition table
	   s   create a new empty Sun partition table

	""")

# Menu experto

def menu_experto ():
	print("""
	Help (expert commands):

  DOS (MBR)
   b   move beginning of data in a partition
   i   change the disk identifier

  Xeometría
   c   change number of cylinders
   h   change number of heads
   s   change number of sectors/track

  Generic
   p   print the partition table
   v   verify the partition table
   d   print the raw data of the first sector from the device
   D   print the raw data of the disklabel from the device
   f   fix partitions order
   m   print this menu

  Save & Exit
   q   quit without saving changes
   r   return to main menu
	""")


def is_hex(s):
    try:
        int(s, 16)
        return True
    except ValueError:
        return False



l_part_activas = [1, 1, 0, 0, 1]
DOS_Comp_flag = None
u_flag = None

mensaxe_benvida()

x = None

while x != "q" :

	x = input("\nInsert a command (m for help): ")



	if x == "a":

		mensaxe = "Partition number (1, 2, 5, default 5): "

		if l_part_activas[2] == 1:
			mensaxe = "Partition number (1-3, 5, default 5): "

		if l_part_activas[3] == 1:
			mensaxe = "Partition number (1, 2, 4, 5, default 5): "

		if (l_part_activas[2] == 1 and l_part_activas[3] == 1):
			mensaxe = "Partition number (1-4, 5, default 5): "

		a_number = int(input(mensaxe))
		if a_number == 1:

			if (l_part_activas[0] == 0):
				print("\nThe bootable flag on partition 1 is enabled now.")
				l_part_activas[0] = 1


			elif (l_part_activas[0] == 1):
				print("\nThe bootable flag on partition 1 is disabled now.")
				l_part_activas[0] = 0

		elif a_number == 2:

			if (l_part_activas[1] == 0):
				print("\nThe bootable flag on partition 2 is enabled now.")
				l_part_activas[1] = 1


			elif (l_part_activas[1] == 1):
				print("""\nPartition 2: is an extended partition.

	The bootable flag on partition 2 is enabled now.""")
				l_part_activas[1] = 0


		elif a_number == 3:

			if (l_part_activas[2] == 0):
				print("\nThe bootable flag on partition 3 is enabled now.")
				l_part_activas[2] = 1


			elif (l_part_activas[2] == 1):
				print("\nThe bootable flag on partition 3 is disabled now.")
				l_part_activas[2] = 0

		elif a_number == 4:

			if (l_part_activas[3] == 0):
				print("\nThe bootable flag on partition 4 is enabled now.")
				l_part_activas[3] = 1


			elif (l_part_activas[3] == 1):
				print("\nThe bootable flag on partition 4 is disabled now.")
				l_part_activas[3] = 0

		elif a_number == 5 or a_number == "":

			if (l_part_activas[4] == 0):
				print("\nThe bootable flag on partition 5 is enabled now.")
				l_part_activas[4] = 1


			elif (l_part_activas[4] == 1):
				print("\nThe bootable flag on partition 5 is disabled now.")
				l_part_activas[4] = 0

		else:
			print("\nValue out of range.")



	elif x == "b":

		print("""
There is no *BSD partition on /dev/sda.

The device (null) does not contain BSD disklabel.
		""")
		b_option = input("Do you want to create a BSD disklabel? [Y]es/[N]o: ")


		if b_option == "Y":
			print("\nThere is no *BSD partition on /dev/sda.")
		else:
			pass

	elif x == "c":


		if DOS_Comp_flag == None :
			print("DOS Compatibility flag is set (DEPRECATED!)")
			DOS_Comp_flag = True

		elif DOS_Comp_flag == True :
			print("DOS Compatibility flag not set")
			DOS_Comp_flag = None

	elif x == "d":
		d_number = input("Partition number (1,2,5, default 5): ")
		if d_number == "1" or d_number == "2" or d_number == "5" :
			print("\nThe partition " + d_number + " is deleted.")
		elif d_number == "":
			print("\nThe partition 5 (by default) is deleted.")
		elif d_number == "3" or d_number == "4":
			print("\nPartition " + d_number + " does not exist!")
		else:
			print("\nValue out of range or you must insert a numeric value.")

	elif x == "F":
		menu_F()

	elif x == "l":
		menu_l()

	elif x == "n":
		print("""
All space for primary partitions is in use.
Adding logical partition 6
No free sectors available.
		""")

	elif x == "p":
		menu_p()


	elif x == "t":
		t_number = str(input("Partition number (1,2,5, default 5): "))
		if t_number == "1" or t_number == "5" or t_number == "" :

			if t_number == "":
				t_number = "5"

			t_type_hex = str(input("Partition type (type l to list all types): "))
			#t_type_dec = int(t_type_hex, 16)

			if t_type_hex == "l":
				menu_l()

#			É necesario contral-la excepción hexadecimal, cando un valor non usa caracteres hexadecimais !!!

			elif is_hex(t_type_hex) == False:
				print("""
Probably, you have inserted a non hexadecimal value.

Type 0 means free space to many systems. Having partitions of type 0 is probably unwise.

Changed type of partition 'Linux swap / Solaris' to 'unknown'.

				""")

			elif int(t_type_hex, 16) > 255:
				print("""
Probably, you are out of range (00-ff).

Type 0 means free space to many systems. Having partitions of type 0 is probably unwise.

Changed type of partition 'Empty' to 'unknown'.
				""")

			elif int(t_type_hex, 16) >= 1 and int(t_type_hex, 16) <= 255:
				print("\nThe partition " + t_number + " type changed successfully.")

			elif int(t_type_hex, 16) == 0:
				print("""
Type 0 means free space to many systems. Having partitions of type 0 is probably unwise.

Changed type of partition 'Empty' to 'Empty'.
				""")

			else:
				print("\nChanged type of partition 'unknown' to 'unknown'.")

		elif t_number == "2" :
			print("""
Cannot change type of the extended partition which is already used by logical partitions. Delete logical partitions first.

Type of partition 2 is unchanged: Extended.
			""")

		elif t_number == "3" or t_number == "4":
			print("\nPartition " + str(t_number) + " does not exist!")
		else:
			print("\nValue out of range.")


	elif x == "v":
		print("\nRemaining 4095 unallocated 512-b.")


	elif x == "i":
		i_number = str(input("Partition number (1,2,5, default 5): "))
		if i_number == "1":
			menu_info_sda1()
		elif i_number == "2":
			menu_info_sda2()
		elif i_number == "3" or i_number == "4":
			print("\nPartition " + i_number + " does not exist!")
		elif i_number == "5" or i_number == "":
			menu_info_sda5()
		else:
			print("\nValue out of range.")

	elif x == "m":
		menu_opcion()

	elif x == "u":
		print()

		if u_flag == None :
			print("\nChanging display/entry units to cylinders (DEPRECATED!")
			u_flag = True

		elif u_flag == True :
			print("Changing display/entry units to sectors.")
			u_flag = None

	# Expert menu

	elif x == "x":

		x_expert = None

		while x_expert != "r" :

			x_expert = input("\nExpert command (m for help): ")

			if x_expert == "b":

				b_exp_number = input("Partition number (1,2,5, default 5): ")
				if b_exp_number == "1" or b_exp_number == "2" or b_exp_number == "5" or b_exp_number == "":

					if b_exp_number == "":
						b_exp_number = "5"

					print("\nNew beginning of data (48236543-52426751, default 48236544): ")
					print("\nBeginning of data moved successfully in partition " + b_exp_number + "." )


				elif b_exp_number == "3" or b_exp_number == "4":
					print("\nPartition " + str(b_exp_number) + " does not exist!")

				else:
					print("\nValue out of range.")

			elif x_expert == "i":

				i_exp_number = input("Enter the new disk identifier: ")

				if i_exp_number == "xabier":
						print("\nDisk identifier changed")
				else:
					print("\nIncorrect value.")


			elif x_expert == "c":

				c_exp_number = str(input("Number of cylinders (1-1048576, default 3263): "))

				if c_exp_number == "":
					c_exp_number = "3263"

				if int(c_exp_number) >= 1 and int(c_exp_number) <= 1048576:
						print("\nNumber of cylinders have changed to: " + str(c_exp_number) )
				else:
					print("\nIncorrect value.")


			elif x_expert == "h":

				h_exp_number = str(input("Number of heads (1-256, default 255): "))

				if h_exp_number == "":
					h_exp_number = "255"

				if int(h_exp_number) >= 1 and int(h_exp_number) <= 256:
					print("\nNumber of heads have changed to: " + str(h_exp_number) )
				else:
					print("\nIncorrect value.")

			elif x_expert == "s":

				s_exp_number = str(input("Number of sectors (1-63, default 63): "))

				if s_exp_number == "":
					s_exp_number = "63"

				if int(s_exp_number) >= 1 and int(s_exp_number) <= 63:
					print("\nNumber of sectors have changed to: " + str(s_exp_number) )
				else:
					print("\nIncorrect value.")


			elif x_expert == "p":
				menu_p_exp ()

			elif x_expert == "v":
				print("\nRemaining 4095 unallocated 512-byte sectors.")

			elif x_expert == "d":
				menu_exp_d ()

			elif x_expert == "D":
				menu_exp_D ()

			elif x_expert == "f":
				print("""
Nothing to do. Ordering is correct already.
Failed to fix partitions order.
				""")

			elif x_expert == "m":
				menu_experto ()

			elif x_expert == "r":
				pass

			else:
				print("\n", x_expert, ": unknown command\n")


	elif x == "I":

		I_exp_number = str(input("Enter script file name:  "))

		if I_exp_number == "url_script":
			print("\nThe script was successfully executed.")
		else:
			print("\nCannot open : Non hai tal ficheiro ou directorio")


	elif x == "O":

		O_exp_number = input("Enter script file name:  ")

		if O_exp_number == "":
			print("\nCannot open : Non hai tal ficheiro ou directorio.")
		else:
			print("\nScript successfully saved.")


	elif x == "w":
		print("\nTable written successfully to disk and exit.\n")
		break

	elif x == "q":
		pass


	elif x == "g":
		print("\nCreated a new GPT disklabel (GUID: 518EDE7E-4CA6-48E2-A6DE-2A406E4EBAF3).")

	elif x == "G":
		print("""
Created a new partition 11 of type 'SGI volume' and of size 25 GiB.
Created a new partition 9 of type 'SGI volhdr' and of size 2 MiB.
Created a new SGI disklabel.
		""")

	elif x == "o":
		print("\nCreated a new DOS disklabel with disk identifier 0xc8926df6.")

	elif x == "s":
		print("""
Created a new partition 1 of type 'Linux native' and of size 25 GiB.
Created a new partition 2 of type 'Linux swap' and of size 47,1 MiB.
Created a new partition 3 of type 'Whole disk' and of size 25 GiB.
Created a new Sun disklabel.
		""")

	else:
		print("\n", x, ": unknown command\n")
